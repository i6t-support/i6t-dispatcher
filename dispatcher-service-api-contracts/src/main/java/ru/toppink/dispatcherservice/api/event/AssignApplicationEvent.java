package ru.toppink.dispatcherservice.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.common.kafka.OrdinalEvent;
import ru.toppink.common.kafka.OrdinalId;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssignApplicationEvent implements OrdinalEvent {

    @OrdinalId
    private UUID applicationId;
    private String executor;
}

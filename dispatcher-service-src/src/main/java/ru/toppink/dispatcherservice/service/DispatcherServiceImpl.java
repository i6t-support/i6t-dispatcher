package ru.toppink.dispatcherservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.api.event.ApplicationChangeStatusEvent;
import ru.toppink.common.exception.NotFoundException;
import ru.toppink.common.exception.UserNotFoundException;
import ru.toppink.common.kafka.KafkaSender;
import ru.toppink.common.securities.dao.Role;
import ru.toppink.common.utils.Optionals;
import ru.toppink.dispatcherservice.api.event.AssignApplicationEvent;
import ru.toppink.userservice.api.dto.UserDto;
import ru.toppink.userservice.api.feign.UserClient;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class DispatcherServiceImpl implements DispatcherService {

    private static final Set<ApplicationStatus> DELEGATE_TO_OPERATOR = Set.of(
            ApplicationStatus.DELEGATE_TO_OPERATOR_FOR_CLOSE,
            ApplicationStatus.DELEGATE_TO_OPERATOR,
            ApplicationStatus.OPERATOR_SEARCH
    );

    private static final Set<ApplicationStatus> DELEGATE_TO_CLIENT = Set.of(
            ApplicationStatus.WAITING_APPROVE_CLIENT
    );

    private final UserClient userClient;
    private final KafkaSender kafkaSender;

    @Override
    public void resolveApplicationChangeStatus(ApplicationChangeStatusEvent payload) throws NotFoundException {
        log.debug("Start resolve application change status: {}", payload);

        final UUID applicationId = payload.getApplicationId();
        final String client = payload.getClient();
        final String operator = payload.getOperator();
        final ApplicationStatus status = payload.getStatus();

        Optional<String> maybeCandidate = Optional.empty();
        if (DELEGATE_TO_OPERATOR.contains(status)) {
            maybeCandidate = findCandidateDelegateToOperator(applicationId, operator);
        } else if (ApplicationStatus.DELEGATE_TO_TECH_SPEC.equals(status)) {
            maybeCandidate = findCandidateDelegateToTechSpec(applicationId);
        } else if (ApplicationStatus.DELEGATE_TO_SUPPORT.equals(status)) {
            maybeCandidate = findCandidateDelegateToSupport(applicationId);
        } else {
            return;
        }

        String candidate = maybeCandidate.orElseThrow(() -> new NotFoundException("Candidate for application = "
                + applicationId + " and status = " + status + " can't be found"));

        AssignApplicationEvent event = AssignApplicationEvent.builder()
                .applicationId(applicationId)
                .executor(candidate)
                .build();

        kafkaSender.sendToDispatcherOut(event);
    }

    @Override
    public Optional<String> findCandidateDelegateToOperator(UUID applicationId, String operator) {
        if (operator != null) {
            return Optional.of(operator);
        }

        List<UserDto> users = findByRole(Role.OPERATOR);
        return users.size() != 0 ? Optional.ofNullable(users.get(0).getLogin()) : Optional.empty();
    }

    @Override
    public Optional<String> findCandidateDelegateToTechSpec(UUID applicationId) {
        List<UserDto> users = findByRole(Role.TECH_SPEC);
        return users.size() != 0 ? Optional.ofNullable(users.get(0).getLogin()) : Optional.empty();
    }

    @Override
    public Optional<String> findCandidateDelegateToSupport(UUID applicationId) {
        List<UserDto> users = findByRole(Role.SUPPORT);
        return users.size() != 0 ? Optional.ofNullable(users.get(0).getLogin()) : Optional.empty();
    }

    @Override
    public Optional<String> findCandidateDelegateToClient(UUID applicationId, String loginClient) {
        return Optional.ofNullable(loginClient);
    }

    private List<UserDto> findByRole(Role role) {
        log.debug("Start search users with role = {}", role);
        return Optionals.of(
                userClient.findByRole(role),
                result -> log.debug("Search users finished: {}", result)
        );
    }
}

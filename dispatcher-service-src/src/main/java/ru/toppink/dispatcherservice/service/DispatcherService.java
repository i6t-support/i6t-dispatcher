package ru.toppink.dispatcherservice.service;

import ru.toppink.applicationservice.api.event.ApplicationChangeStatusEvent;
import java.util.Optional;
import java.util.UUID;

public interface DispatcherService {

    void resolveApplicationChangeStatus(ApplicationChangeStatusEvent payload);

    Optional<String> findCandidateDelegateToOperator(UUID applicationId, String operator);

    Optional<String> findCandidateDelegateToTechSpec(UUID applicationId);

    Optional<String> findCandidateDelegateToSupport(UUID applicationId);

    Optional<String> findCandidateDelegateToClient(UUID applicationId, String loginClient);
}

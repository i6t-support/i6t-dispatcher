package ru.toppink.dispatcherservice.config;

import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import ru.toppink.common.utils.TuzRequestInterceptor;
import ru.toppink.userservice.api.feign.UserClient;

@Configuration
@EnableFeignClients(clients = {UserClient.class})
public class FeignConfig {

    @Value("${tuz}")
    private String tuz;

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    @Bean
    public RequestInterceptor tuzRequestInterceptor() {
        return template -> template.header(HttpHeaders.AUTHORIZATION, "Bearer " + tuz);
    }
}

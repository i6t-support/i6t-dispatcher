package ru.toppink.dispatcherservice.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.api.event.ApplicationChangeStatusEvent;
import ru.toppink.dispatcherservice.service.DispatcherService;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserCreateApplicationListener implements ApplicationListener<PayloadApplicationEvent<ApplicationChangeStatusEvent>> {

    private final DispatcherService dispatcherService;

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<ApplicationChangeStatusEvent> event) {
        dispatcherService.resolveApplicationChangeStatus(event.getPayload());
    }
}
